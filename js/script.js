



let menu = document.querySelector('#menu-bar');
let navbar = document.querySelector('.navbar');

menu.onclick =() =>{
    menu.classList.toggle('fa-times');
    navbar.classList.toggle('active');
}

let slides = document.querySelectorAll('.slide-container');
let index = 0;

function next(){
    slides[index].classList.remove('active');
    index = (index + 1) % slides.length;
    slides[index].classList.add('active');
}

function prev(){
    slides[index].classList.remove('active');
    index = (index - 1 + slides.length) % slides.length;
    slides[index].classList.add('active');
}

document.querySelectorAll('.featured-image-1').forEach(image_1 =>{
    image_1.addEventListener('click', () =>{
        var src = image_1.getAttribute('src');
        document.querySelector('.big-image-1').src = src;
    });
});

document.querySelectorAll('.featured-image-2').forEach(image_2 =>{
    image_2.addEventListener('click', () =>{
        var src = image_2.getAttribute('src');
        document.querySelector('.big-image-2').src = src;
    });
});

document.querySelectorAll('.featured-image-3').forEach(image_3 =>{
    image_3.addEventListener('click', () =>{
        var src = image_3.getAttribute('src');
        document.querySelector('.big-image-3').src = src;
    });
});



$(function(){
   
    $('#sleepcalc').combodate({
      customClass: 'form-control'
    });  
    
    $('#sleepcalc, .hour, .minute').change( function() {
      updatetime()
    })
    
    
    function updatetime(){
      var interval = moment.duration("01:30:00")
      var secondinterval = moment.duration("03:00:00")
      
      var selectedTime = $('#sleepcalc').combodate('getValue', null)
      
      var time = moment.duration("09:15:00")
      var date = moment(selectedTime)
      date.subtract(time)
  
      result1 = moment(date).format("h:mm a")
      result2 = moment(date).add(interval).format("h:mm a")
      result3 = moment(date).add(secondinterval).format("h:mm a")
  
      $('#result1').text(result1).addClass('loaded')
      $('#result2').text(result2).addClass('loaded')
      $('#result3').text(result3).addClass('loaded')
      
      setTimeout(function() {
          $('#result1, #result2, #result3').removeClass('loaded');
      }, 1500)
      
    }              
  
    updatetime()
    
  });